import { Component, OnInit } from '@angular/core';
import { QuestionsService } from './../questions.service'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  categories = [];

  constructor(
    private questionsService : QuestionsService,
    private router : Router,
    private activatedRoute : ActivatedRoute ,
  ) { }

  ngOnInit() {
    this.categories = this.questionsService.getCategory()
  }

}
