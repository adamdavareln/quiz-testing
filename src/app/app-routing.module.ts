import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LevelComponent } from './level/level.component'
import { QuestionComponent } from './question/question.component'
import { CategoryComponent } from './category/category.component'
import { HomeComponent } from './home/home.component'

const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'quiz/:category',component:LevelComponent},
  {path:'play',component:QuestionComponent},
  {path:'quiz',component:CategoryComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
