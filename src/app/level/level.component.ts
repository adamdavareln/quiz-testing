import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { QuestionsService } from '../questions.service';

@Component({
  selector: 'app-level',
  templateUrl: './level.component.html',
  styleUrls: ['./level.component.css']
})
export class LevelComponent implements OnInit {

  category;
  // levelArray;

  constructor(
    private route : ActivatedRoute,
    private questionsService : QuestionsService,
  ) { }

  ngOnInit() {
    this.category = this.route.snapshot.paramMap.get('category')
    // this.levelArray = this.questionsService.getLevel(this.category)
  }

}
