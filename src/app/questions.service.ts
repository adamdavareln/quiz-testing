import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService {

  questions = [
    {
      'category' : 'electronic',
      'questions' : {
        //1-5
        'beginner':[
          'cable',
          'phone',
          'mouse',
          'radio',
          'lcd',
          'mic',
          'cctv',
          'fan',
          'iron',
          'mixer',
        ],
        //6-8
        'intermediate':[
          'remote',
          'juicer',
          'laptop',
          'camera',
          'tablet',
          'battery',
          'speaker',
          'computer',
          'charger',
          'printer',
          'earphone',
          'keyboard',
          'recorder',
        ],
        //9++
        'hard':[
          'projector',
          'headphone',
          'microphone',
          'smartphone',
          'microwave',
          'refrigator',
          'humidifier',
          'television',
          'dishwasher',
          'flashlight',
        ]
      }
    },
    {
      'category' : 'foods',
      'questions' : {
        'beginner':[
          'cake',
          'candy',
          'rice',
          'meat',
          'bread',
          'eggs',
          'soups',
          'pasta',
          'chips',
          'trout',
          'kebab',
          'pie',
        ],
        'intermediate':[
          'noodles',
          'chicken',
          'mackerel',
          'cheese',
          'mustard',
          'seafood',
          'lobster',
          'sausage',
          'yogurt',
          'mushroom',
          'avocado'
        ],
        'hard':[
          'chocolate',
          'dumpling',
          'cheeseburger',
          'sandwich',
          'watermelon',
          'starfruit',
          'blueberry',
          'strawberry',
          'passionfruit',
        ]
      },
    }
  ]
  

  getQuestions(category,level){
    let questionsArray = []

    this.questions.forEach(value => {
      if(value.category == category){
        questionsArray = value.questions[level]
      }
    })

    return questionsArray
  }
  
  getCategory(){
    let categoryArray = []
    this.questions.forEach(value => {
      categoryArray.push(value.category)
    })
    return(categoryArray)
  }

  // getLevel(category){
  //   let levelArray = []
  //   this.questions.forEach(value => {
  //     if(value.category == category){
  //       value.questions[0].foreach(value => {
  //         alert(value)
  //       })
  //     }
  //   })

  //   alert(levelArray)

  //   return levelArray
  // }

  constructor() { }

}
