import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';


@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  constructor() { }

  createDb(){
    const questions = [
      'book',
      'laptop',
      'bottle',
      'phone',
      'earphone',
      'bicycle',
      'keyboard',
      'notebook',
      'marker',
      'cable'
    ]

    return {questions}
  }
}
