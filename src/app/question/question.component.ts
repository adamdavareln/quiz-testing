import { Component, OnInit } from '@angular/core';
import { QuestionsService } from './../questions.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  answered_questions = [];
  words = [];
  score = 0;
  highscore;
  timer = 60;
  category;
  level;
  questions;
  selected_question;
  answer;
  interval;
 
  constructor(
    private questionsService : QuestionsService,
    private route : ActivatedRoute,
  ) { }

  ngOnInit() {
    this.category = this.route.snapshot.paramMap.get('category')
    this.level = this.route.snapshot.paramMap.get('level')
    this.getQuestions() 
    this.getHighscore()
  }    
  
  getQuestions = async()=>{
    try{
      this.questions = await this.questionsService.getQuestions(this.category,this.level)
      this.randoming()
      this.startTimer()
    } catch(err){
      alert(err)
    }
  }

  startTimer(){
    this.interval = setInterval(()=>{
      if(this.timer >= 1){
        this.timer--;
      } else {
        clearInterval(this.interval)
        this.setHighscore()
      }
    },1000)
  }

  async getHighscore(){
    try{
      this.highscore = await localStorage.getItem('highscore');
    } catch (error){
      console.log(error)
    }
  }

  async setHighscore(){
    try{
      if(localStorage.getItem('highscore')){
        let res = await localStorage.getItem('highscore')
        if(+res < this.score){
          await localStorage.setItem('highscore',this.score.toString())
        }
      } else {
        await localStorage.setItem('highscore',this.score.toString())
      }
    } catch(err){
      alert(err)
    }
  }
  
  randoming(){
    let shuffledWord;
    this.selected_question = Math.floor(Math.random() * this.questions.length);
    do{
      shuffledWord = this.shuffleWord(this.questions[this.selected_question]);
    }while(shuffledWord == this.questions[this.selected_question])
    this.words = shuffledWord.split('')
  }

  shuffleWord(a){
    let j,x,i,c = '';
    let b = a.split('');
    for(i = b.length - 1 ; i > 0; i--){
      j = Math.floor(Math.random()*(i+1));
      x = b[i];
      b[i] = b[j];
      b[j] = x;
    }
    for(i = 0;i < b.length;i++){
      c += '' + b[i];
    }
    return c ;
  }

  checkAnswer(){
    let selectedQuestion = this.selected_question
    let answer = this.answer.toLowerCase()
    if(answer == this.questions[this.selected_question].toLowerCase()){
      alert("Great!")
      this.score += 10
      this.answer = ""
      this.questions.splice(this.questions.indexOf(answer),1);
      
      if(this.questions.length > 0){
        this.randoming()
      } else {
        alert('congratulation')
      }

    } else {
      alert("Wrong answer, try again!")
    }
  }
}
